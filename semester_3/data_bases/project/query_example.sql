drop database if exists etaireia;

create database etaireia;
use etaireia;

CREATE TABLE ypallhlos (
    hlikia INTEGER,
    hmerominia_gennhshs DATE,
    a_t VARCHAR(20),
    onoma VARCHAR(20),
    eponumo VARCHAR(20),
    thl DOUBLE,
    odos VARCHAR(50),
    ar INTEGER,
    polh VARCHAR(20),
    misthos INTEGER,
    eidikothta VARCHAR(50),
    hmeromhnia_proslhpshs DATE,
    PRIMARY KEY (a_t)
);

CREATE TABLE dioikhtikoi (
    ptuxio VARCHAR(50),
    titlos VARCHAR(50),
    etos_kthshs DATE,
    a_t VARCHAR(20),
    PRIMARY KEY (a_t),
    CONSTRAINT FOREIGN KEY (a_t)
        REFERENCES ypallhlos (a_t)
);

CREATE TABLE mhxanikoi (
    eth_prouphresias INTEGER,
    hmeromhnia_askhshs_epaggelmatos DATE,
    a_t VARCHAR(20),
    PRIMARY KEY (a_t),
    CONSTRAINT FOREIGN KEY (a_t)
        REFERENCES ypallhlos (a_t)
);

CREATE TABLE grammateis (
    arithmos_seminariwn INTEGER,
    a_t VARCHAR(20),
    PRIMARY KEY (a_t),
    CONSTRAINT FOREIGN KEY (a_t)
        REFERENCES ypallhlos (a_t)
);

CREATE TABLE logistes (
    epipedo_eggrafhs_sto_oikonomiko_epimelhthrio VARCHAR(20),
    a_t VARCHAR(20),
    PRIMARY KEY (a_t),
    CONSTRAINT FOREIGN KEY (a_t)
        REFERENCES ypallhlos (a_t)
);

CREATE TABLE prostateuomena_melh (
    onoma VARCHAR(20),
    fulo VARCHAR(1),
    hlikia DATE,
    a_t VARCHAR(20),
    PRIMARY KEY (a_t),
    CONSTRAINT FOREIGN KEY (a_t)
        REFERENCES ypallhlos (a_t)
);

CREATE TABLE grafeia (
    arithmos INTEGER,
    onoma VARCHAR(20),
    thlefwno INTEGER,
    odos VARCHAR(20),
    polh VARCHAR(20),
    arithmos_dieuthinshs INTEGER,
    PRIMARY KEY (arithmos)
);

CREATE TABLE fusika_proswpa (
    onoma VARCHAR(20),
    epwnumo VARCHAR(20),
    a_t VARCHAR(20),
    afm INTEGER,
    PRIMARY KEY (a_t , afm),
    CONSTRAINT FOREIGN KEY (a_t)
        REFERENCES ypallhlos (a_t)
);

CREATE TABLE pelates (
    arithmos_forologikou_mhtrwou INTEGER,
    thlefwno INTEGER,
    odos VARCHAR(20),
    arithmos INTEGER,
    tk INTEGER,
    polh VARCHAR(20),
    PRIMARY KEY (arithmos_forologikou_mhtrwou)
);

CREATE TABLE etaireies (
    afm INTEGER,
    epwnumia VARCHAR(20),
    PRIMARY KEY (afm)
);

CREATE TABLE diadromes (
    afethria VARCHAR(20),
    arithmos_diadromhs INTEGER,
    proorismos VARCHAR(20),
    kostos FLOAT,
    timh FLOAT,
    hmeromhnia_enarkshs DATE,
    diarkeia TIME,
    PRIMARY KEY (arithmos_diadromhs)
);

CREATE TABLE autokinhta (
    kubika INTEGER,
    arithmos INTEGER,
    hmeromhnia_kukloforias DATE,
    PRIMARY KEY (arithmos)
);

CREATE TABLE epibatika (
    arithmos_thesewn INTEGER,
    arithmos INTEGER,
    PRIMARY KEY (arithmos),
    CONSTRAINT FOREIGN KEY (arithmos)
        REFERENCES autokinhta (arithmos)
);

CREATE TABLE forthga (
    xwrhtikothta INTEGER,
    arithmos INTEGER,
    PRIMARY KEY (arithmos),
    CONSTRAINT FOREIGN KEY (arithmos)
        REFERENCES autokinhta (arithmos)
);

CREATE TABLE pwlhseis_proiontwn (
    arithmos INTEGER,
    hmeromhnia DATE,
    hmeromhnia_diekperaiwshs DATE,
    PRIMARY KEY (arithmos)
);

CREATE TABLE sundeetai (
    arithmos_pwlhshs_proiontwn INTEGER,
    arithmos_diadromhs INTEGER,
    afm_pelatwn INTEGER,
    PRIMARY KEY (arithmos_diadromhs , afm_pelatwn),
    CONSTRAINT FOREIGN KEY (arithmos_diadromhs)
        REFERENCES diadromes (arithmos_diadromhs),
    CONSTRAINT FOREIGN KEY (afm_pelatwn)
        REFERENCES pelates (arithmos_forologikou_mhtrwou)
);

CREATE TABLE ektelei (
    kostos FLOAT,
    arithmos_autokinhtou INTEGER,
    arithmos_diadromhs INTEGER,
    PRIMARY KEY (arithmos_autokinhtou , arithmos_diadromhs),
    CONSTRAINT FOREIGN KEY (arithmos_autokinhtou)
        REFERENCES autokinhta (arithmos),
    CONSTRAINT FOREIGN KEY (arithmos_diadromhs)
        REFERENCES diadromes (arithmos_diadromhs)
);

insert into ypallhlos(hlikia, hmerominia_gennhshs, a_t, onoma, eponumo, thl, odos, ar, polh, misthos, eidikothta, hmeromhnia_proslhpshs)
values (21, '1998/4/25', '89-500-8420', 'Jhon', 'Jago', '6901537856', '50 Manitowish Park', 1, 'London', '1387.04', 'Legal Assistant', '2019/6/30'),
(44, '1975/5/4', '18-131-4153', 'Margaret', 'Dincke', '6922805407', '772 Petterle Crossing', 2, 'Anling', '1050.11', 'GIS Technical Architect', '2001/2/24'),
(47, '1978/4/23', '66-208-2181', 'Loran', 'Rennocks', '6995889434', '9 Sunbrook Junction', 3, 'Krynice', '1164.13', 'Information Systems Manager', '2000/5/14'),
(27, '1992/5/11', '14-073-2479', 'Make', 'Jollye', '6909088742', '89 Hazelcrest Hill', 4, 'Nanqi', '836.08', 'Senior Financial Analyst', '2015/2/12'),
(23, '1996/2/27', '26-351-2592', 'Frank', 'Warburton', '6954222549', '87 Village Lane', 5, 'Colorado Springs', '1079.31', 'Nuclear Power Engineer', '2019/5/12');

insert into dioikhtikoi(ptuxio, titlos, etos_kthshs, a_t) 
values ('University of South Dakota', 'Assistant Professor', '2018/7/7', '89-500-8420'),
('Majmaah University', 'Recruiting Manager', '1997/5/5', '18-131-4153'),
('University Nacional', 'Human Resources Assistant III', '1995/1/22', '66-208-2181'),
('Technological Education Institute of Serres', 'Programmer I', '2014/2/7', '14-073-2479'),
('Pandit Ravi Shankar Shukla University', 'Civil Engineer', '2017/7/9', '26-351-2592');

insert into mhxanikoi(eth_prouphresias, hmeromhnia_askhshs_epaggelmatos, a_t)
values (1, '2019/6/30', '89-500-8420'),
(4, '2001/2/24', '18-131-4153'),
(5, '2000/5/14', '66-208-2181'),
(1, '2015/2/12', '14-073-2479'),
(1, '2019/5/12', '26-351-2592');

insert into grammateis(arithmos_seminariwn, a_t)
values (1, '89-500-8420'),
(10, '18-131-4153'),
(12, '66-208-2181'),
(3, '14-073-2479'),
(2, '26-351-2592');

insert into logistes(epipedo_eggrafhs_sto_oikonomiko_epimelhthrio, a_t)
values ('Epipedo 1', '89-500-8420'),
('Epipedo 5', '18-131-4153'),
('Epipedo 5', '66-208-2181'),
('Epipedo 2', '14-073-2479'),
('epipedo 1','26-351-2592');

UPDATE ypallhlos 
SET 
    misthos = misthos * 1.1
WHERE
    (CURDATE() - hmeromhnia_proslhpshs > 20);

DELETE FROM diadromes 
WHERE
    (CURDATE() - hmeromhnia_enarkshs > 2);