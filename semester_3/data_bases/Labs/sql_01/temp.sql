drop database if exists temp;
create database temp;
use temp;
create table student(
  ssid integer,
  fName varchar (20),
  lName varchar (20),
  bDate date,
  addr varchar (20),
  sex char(1),
  sum_credit integer,
  s_iid integer,
  dno integer,
  primary key (ssid)
);
create table department(
  dNumber integer,
  dName varchar (40),
  miid integer,
  primary key (dNumber)
);
create table lesson(
  lNumber integer,
  lName varchar (20),
  credits integer,
  dNumber integer,
  primary key (lNumber)
);
create table watch(
  sid integer,
  lNumber integer,
  grade integer,
  primary key (sid, lNumber)
);
create table instructor(
  iid integer,
  fName varchar (20),
  lName varchar (20),
  sex char (1),
  bDate date,
  salary float,
  dNumber integer,
  primary key (iid)
);
create table teach(
  iid integer,
  lNumber integer,
  primary key (iid, lNumber)
);
alter table student
add
  constraint foreign key (s_iid) references instructor(iid);
alter table student
add
  constraint foreign key (dno) references department(dNumber);
alter table lesson
add
  constraint foreign key (dNumber) references department(dNumber);
alter table watch
add
  constraint foreign key (sid) references student(ssid);
alter table watch
add
  constraint foreign key (lNumber) references lesson(lNumber);
alter table instructor
add
  constraint foreign key (dNumber) references department(dNumber);
alter table teach
add
  constraint foreign key (iid) references instructor(iid);
alter table teach
add
  constraint foreign key (lNumber) references lesson(lNumber);
insert into student(
    ssid,
    fName,
    lName,
    bDate,
    addr,
    sex,
    sum_credit,
    s_iid,
    dno
  )
values
  (
    100,
    'Nikos',
    'Papas',
    '1998-02-28',
    'Ikarou 1',
    'M',
    30,
    null,
    null
  ),
  (
    101,
    'Kostas',
    'Peios',
    '2000-01-31',
    'Kritis 23',
    'M',
    13,
    null,
    null
  ),
  (
    103,
    'Manolis',
    'Vaoios',
    '1999-03-23',
    'Xaniwn 2',
    'M',
    32,
    null,
    null
  ),
  (
    105,
    'Stelios',
    'Stamoulis',
    '1998-08-28',
    'Skoula 12',
    'M',
    37,
    null,
    null
  ),
  (
    106,
    'Maria',
    'Stefanou',
    '2000-10-31',
    'Itanou 3',
    'F',
    17,
    null,
    null
  ),
  (
    110,
    'George',
    'Panou',
    '1999-08-23',
    'Ikarou 8',
    'M',
    27,
    null,
    null
  ),
  (
    111,
    'Meropi',
    'Petrou',
    '1997-08-20',
    'Itanou 2',
    'F',
    57,
    null,
    null
  );
insert into department(dNumber, dName, miid)
values
  (1, 'Computer Science', 3),
  (2, 'Business administrator', 4);
insert into lesson(lNumber, lName, credits, dNumber)
values
  (1000, 'Programming', 10, 1),
  (1200, 'Databases', 8, 1),
  (1700, 'Internet', 7, 1),
  (1800, 'Management', 9, 1);
insert into watch(sid, lNumber, grade)
values
  (111, 1000, 6),
  (100, 1200, 7),
  (103, 1700, 3),
  (100, 1000, 4),
  (101, 1700, 5),
  (110, 1800, 3),
  (111, 1800, 10),
  (105, 1800, 8),
  (106, 1200, 7.5);
insert into instructor(iid, fName, lName, sex, bDate, salary, dNumber)
values
  (
    1,
    'Kostas',
    'Papadopoulos',
    'M',
    '1970-03-12',
    1700,
    1
  ),
  (
    2,
    'Maria',
    'Vassalou',
    'F',
    '1972-08-31',
    1300,
    2
  ),
  (
    3,
    'Stelios',
    'Perastikos',
    'M',
    '1969-09-23',
    1200,
    2
  ),
  (
    4,
    'Nikos',
    'Papadopoulos',
    'M',
    '1979-08-28',
    1540.9,
    1
  );
insert into teach(iid, lNumber)
values
  (1, 1000),
  (1, 1200),
  (2, 1700),
  (3, 1000),
  (3, 1800),
  (4, 1800);
