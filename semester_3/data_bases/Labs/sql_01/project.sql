drop database if exists project;
create database project;
use project;

CREATE TABLE worker (
    id CHAR(8),
    first_name VARCHAR(20),
    last_name VARCHAR(20),
    location VARCHAR(40),
    salary FLOAT,
    employed_date DATE,
    birth_date DATE,
    age DATE,
    PRIMARY KEY (id)
);
CREATE TABLE administrative (
    id CHAR(8),
    title VARCHAR(30),
    PRIMARY KEY (id),
    CONSTRAINT FOREIGN KEY (id)
        REFERENCES worker (id)
);
CREATE TABLE accountant (
    id CHAR(8),
    signup_level VARCHAR(30),
    PRIMARY KEY (id),
    CONSTRAINT FOREIGN KEY (id)
        REFERENCES worker (id)
);
CREATE TABLE secretary (
    id CHAR(8),
    number_of_seminars_attended INTEGER,
    PRIMARY KEY (id),
    CONSTRAINT FOREIGN KEY (id)
        REFERENCES worker (id)
);
CREATE TABLE engineer (
    id CHAR(8),
    on_job_time INTEGER,
    started_job DATE,
    PRIMARY KEY (id),
    CONSTRAINT FOREIGN KEY (id)
        REFERENCES worker (id)
);
CREATE TABLE protected_members (
    fullname VARCHAR(50),
    age INTEGER,
    sex CHAR(1),
    id CHAR(8),
    PRIMARY KEY (id),
    CONSTRAINT FOREIGN KEY (id)
        REFERENCES worker (id)
);
CREATE TABLE offices (
    office_name VARCHAR(20),
    office_number INTEGER,
    phone_numbers INTEGER,
    location VARCHAR(40),
    PRIMARY KEY (office_name , office_number)
);
CREATE TABLE sells (
    sell_number INTEGER,
    sell_date DATE,
    confirmation_date DATE,
    PRIMARY KEY (sell_number)
);
CREATE TABLE consumer (
    consumer_id INTEGER,
    phone_numbers INTEGER,
    location VARCHAR(40),
    PRIMARY KEY (consumer_id)
);
CREATE TABLE person (
    person_id INTEGER,
    first_name VARCHAR(20),
    last_name VARCHAR(30),
    id CHAR(8),
    PRIMARY KEY (person_id),
    CONSTRAINT FOREIGN KEY (id)
        REFERENCES consumer (consumer_id)
);
CREATE TABLE company (
    company_id INTEGER,
    company_name VARCHAR(20),
    PRIMARY KEY (company_id),
    CONSTRAINT FOREIGN KEY (id)
        REFERENCES consumer (consumer_id)
);
CREATE TABLE routes (
    route_number INTEGER,
    starting_point VARCHAR(40),
    finishing_point VARCHAR(40),
    route_price FLOAT,
    route_cost FLOAT,
    route_date DATE,
    duration DATE,
    PRIMARY KEY (route_number)
);
CREATE TABLE vehicle (
    vehicle_number INTEGER,
    vehicle_date DATE,
    vehicle_size FLOAT,
    PRIMARY KEY (vehicle_number)
);
CREATE TABLE passenger (
    passenger_number INTEGER,
    passenger_seats INTEGER,
    PRIMARY KEY (passenger_vehicle),
    CONSTRAINT FOREIGN KEY (passenger_number)
        REFERENCES vehicle (vehicle_number)
);
CREATE TABLE truck (
    truck_number INTEGER,
    truck_size INTEGER,
    PRIMARY KEY (truck_number),
    CONSTRAINT FOREIGN KEY (truck_number)
        REFERENCES vehicle (vehicle_number)
);

CREATE TABLE connects (
    sell_number INTEGER,
    route_number INTEGER,
    consumer_id INTEGER,
    PRIMARY KEY (route_number , consumer_id),
    CONSTRAINT FOREIGN KEY (route_number)
        REFERENCES routes (route_number),
    CONSTRAINT FOREIGN KEY (consumer_id)
        REFERENCES consumer (consumer_id)
);

CREATE TABLE deal (
    cost FLOAT,
    vehicle_number INTEGER,
    route_number INTEGER,
    PRIMARY KEY (vehicle_number , route_number),
    CONSTRAINT FOREIGN KEY (vehicle_number)
        REFERENCES vehicle (vehicle_number),
    CONSTRAINT FOREIGN KEY (route_number)
        REFERENCES routes (route_number)
);

UPDATE worker 
SET 
    salary = salary * 1.1
WHERE
    (CURDATE() - employed_date > 20);
DELETE FROM routes 
WHERE
    (CURDATE() - route_date > 2);