drop database if exists company;

create database company;
use company;

CREATE TABLE employee (
    FNAME VARCHAR(10),
    MINIT CHAR(1),
    LNAME VARCHAR(15),
    SSN INTEGER,
    BDATE DATE,
    ADDRESS VARCHAR(20),
    SEX CHAR(1),
    SALARY FLOAT,
    SUPERSSN INTEGER,
    DNO INTEGER,
    PRIMARY KEY (SSN)
);

CREATE TABLE department (
    DNAME VARCHAR(10),
    DNUMBER INTEGER,
    MGRSSN INTEGER,
    MGRSTARDATE DATE,
    PRIMARY KEY (DNUMBER),
    CONSTRAINT FOREIGN KEY (MGRSSN)
        REFERENCES employee (ssn)
);
 
CREATE TABLE dept_locations (
    DNUMBER INTEGER,
    DLOCATION VARCHAR(15),
    PRIMARY KEY (DNUMBER , DLOCATION),
    CONSTRAINT FOREIGN KEY (DNUMBER)
        REFERENCES department (DNUMBER)
);

CREATE TABLE project (
    PNAME VARCHAR(10),
    PNUMBER INTEGER,
    PLOCATION VARCHAR(15),
    DNUM INTEGER,
    PRIMARY KEY (PNUMBER),
    CONSTRAINT FOREIGN KEY (DNUM)
        REFERENCES department (DNUMBER)
);

CREATE TABLE works_on (
    ESSN INTEGER,
    PNO INTEGER,
    hours INTEGER,
    PRIMARY KEY (ESSN , PNO),
    CONSTRAINT FOREIGN KEY (ESSN)
        REFERENCES employee (ssn),
    CONSTRAINT FOREIGN KEY (PNO)
        REFERENCES project (PNUMBER)
);

CREATE TABLE dependent (
    essn INTEGER,
    dependentname VARCHAR(25),
    sex CHAR(1),
    bdate DATE,
    relationship VARCHAR(10),
    PRIMARY KEY (essn , dependentname),
    CONSTRAINT FOREIGN KEY (essn)
        REFERENCES employee (ssn)
);


ALTER TABLE employee ADD CONSTRAINT foreign key(superssn) references employee(ssn); 

ALTER table employee ADD CONSTRAINT foreign key(DNO) references department(dnumber); 


insert into employee values ('Nikos','I','Smith',7,'1970-06-13','Ikarou 7','M',1700.73,NULL,NULL);

insert into department values('Research',2,7,'2009-08-31');

insert into project values('nshield',19, 'Stafford',NULL);

insert into works_on values(7,19,20);

insert into dept_locations values(2,'Stafford');


insert into dependent values(7,'Stelios Vairis','M','1996-08-08','son');

insert into employee values ('Manolis','I','Drakos',8,'1965-06-23','Faistou 8','M',1900.93,NULL,2);

UPDATE employee 
SET 
    Dno = 2,
    superssn = 8
WHERE
    ssn = 117;

insert into employee values ('Manolis','M','Pepas',17,'1976-06-13','Ikarou 47','M',1400.73,NULL,NULL);
insert into employee values ('Nikos','I','Smith',27,'1987-09-13','Ikarou 7','M',1900.3,NULL,NULL);
insert into employee values ('Nikos','I','Peios',37,'1960-08-13','Ikarou 7','M',900.7,NULL,NULL);
insert into employee values ('Maria','I','Vairi',9,'1980-06-23','Kalokairinou 7','F',700.73,NULL,NULL);
insert into employee values ('Nikos','I','Vairis',117,'1970-06-13','Ikarou 7','M',1700.73,8,2);

insert into dependent values(7,'Giorgos Vairis','M','1999-09-19','son');
insert into dependent values(27,'Maria Babalh','F','1956-08-08','Mother');
insert into dependent values(37,'Stelios Peios','M','2002-07-28','son');
insert into dependent values(7,'Manolis Vairis','M','1949-08-08','Father');


UPDATE employee 
SET 
    Dno = 2,
    superssn = 8
WHERE
    ssn = 9;



UPDATE employee 
SET 
    Dno = 2,
    superssn = 8
WHERE
    ssn = 37;

insert into department values('Develop',7,27,'2010-10-31');
insert into dept_locations values(7,'Stafford');
insert into dept_locations values(7,'Iraklio');

UPDATE project 
SET 
    DNUM = 7
WHERE
    pnumber = 19;

UPDATE employee 
SET 
    Dno = 7,
    superssn = 27
WHERE
    ssn = 17;


UPDATE employee 
SET 
    Dno = 7
WHERE
    ssn = 27;

insert into project values('towl',9, 'Stafford',2);

insert into works_on values(17,9,10);
insert into works_on values(27,9,15);
insert into works_on values(9,9,23);

insert into project values('dasta',29, 'Iraklio',7);

insert into works_on values(8,29,20);
insert into works_on values(7,29,5);
insert into works_on values(9,29,12);
insert into works_on values(37,29,15);

SELECT 
    BDATE, ADDRESS
FROM
    employee
WHERE
    FNAME = 'Nikos' AND MINIT = 'I'
        AND LNAME = 'Vairis';
        
SELECT
    FNAME, ADDRESS
FROM
    department, employee
WHERE
    DNUMBER = 2 AND MGRSSN = 7;