#define _GNU_SOURCE

// Melethste to 15.10 (semaphores) apo to vivlio Stevens and Rago
// oson afora sem_open/sem_destroy/sem_close, sem_init, sem_wait,
// and sem_post.
//
// Simple producer/consumer program with pthreads which uses
// semaphores to synchronize
//
// Use in Linux:
//     gcc 08_prod_cons.c -lpthread -lrt
//     ./a.out
//
// LAB : Try to extend the code to write
//	 a) a distributed consumer/producer problem
//          (multiple producers/consumers)
//	 b) a distributed pipelined consumer/producer problem
//	    (consumers are also producers for next stage)

#define MAXSIZE 10000000
#define MAXDELAY 2

#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdint.h>

pthread_mutex_t lock;

#include <stdlib.h>

/* #define SHARED 1  - for Solaris - shared between processes */
#define SHARED 0 /* for Linux - shared between threads but not between processes */

void *Producer(void *); /* the two threads */
void *Consumer(void *);

int data[MAXSIZE]; /* shared buffer */
long produced = 0;
long consumed = 0;

sem_t empty, occupied; /* global semaphores */

/* main() -- read command line and create threads, then
             print result when the threads have quit */
int main(int argc, char *argv[])
{
	/* thread ids and attributes */
	pthread_t pid[8], cid[8];
	pthread_attr_t attr;
	pthread_attr_init(&attr);
	pthread_attr_setscope(&attr, PTHREAD_SCOPE_SYSTEM);

	int L = 16;
	sem_init(&empty, SHARED, L);	// producer semaphore (stops after L elements)
	sem_init(&occupied, SHARED, 0); // occupied semaphore (initially stopped)

	int id;
	for (id = 0; id < 8; id++)
	{
		pthread_create(&pid[id], &attr, Producer, NULL);
	}
	for (id = 0; id < 8; id++)
	{
		pthread_create(&cid[id], &attr, Consumer, NULL);
	}

	for (id = 0; id < 8; id++)
	{
		pthread_join(pid[id], NULL);
	}
	for (id = 0; id < 8; id++)
	{
		pthread_join(cid[id], NULL);
	}

	sem_destroy(&empty);
	sem_destroy(&occupied);
}

/* deposit elements into the data buffer */
void *Producer(void *arg)
{
	int i = (intptr_t)arg, x;

	printf("Produce starts producing\n");

	while (produced < MAXSIZE)
	{
		sem_wait(&empty);
		pthread_mutex_lock(&lock);
		data[produced] = produced;
		produced++;
		printf("Producer produced: %li\n", produced);
		fflush(stdout);
		pthread_mutex_unlock(&lock);
		// random delay
		//	x = (int)rand() / 100000000.0;
		//	for (i = 0; i < x; i++)
		//	{
		//		system("sleep 0.001");
		//	}
		sem_post(&occupied);

		if (lrand48() % 2 == 1)
		{
			pthread_yield(); /* random yield to another thread */
		}
	}
}

/* fetch items from the buffer and sum them */
void *Consumer(void *arg)
{

	int i = (intptr_t)arg, x;
	long total = 0;
	printf("Consumer starts consuming\n");

	while (consumed < MAXSIZE)
	{

		sem_wait(&occupied);
		pthread_mutex_lock(&lock);
		total = total + data[consumed];
		consumed++;
		printf("Consumer consumed: %li\n", consumed);
		fflush(stdout);
		pthread_mutex_unlock(&lock);
		// random delay
		//	x = (int)rand() / 100000000.0;
		//	for (i = 0; i < x; i++)
		//	{
		//		system("sleep 0.001");
		//	}
		sem_post(&empty);

		if (lrand48() % 2 == 1)
		{
			pthread_yield(); /* random yield to another thread */
		}
	}
	printf("Elements produced: %li consumed:%li\n", produced, consumed);
}
