//  import libraries
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>

//  define constants
#define N 10000000
#define largest_number N
#define array_size N
#define number_of_threads 8

//  define global variables
int a[array_size], b[array_size], ab[array_size], sum = 0, first_index = 0, last_index;
int buffer = array_size / number_of_threads;

//  define function to randomly populate an array of integers
int random_int_generator(int arr[])
{
    for (int index = 0; index < largest_number; index++)
    {
        arr[index] = rand() % largest_number;
    }
    return 0;
}

//  define function to print the values of an array
int print_array(int arr[], char array_name[])
{
    printf("%s\n", array_name);
    for (int i = 0; i < array_size; i++)
    {
        printf("Item index: %i\tItem value: %i\n", i, arr[i]);
    }
    return (0);
}

//  define function to calculate the dot product of two vectors with multithreaded execution
void *dot_product()
{
    for (int i = first_index; i < last_index; i++)
    {
        ab[i] = a[i] + b[i];
        sum += ab[i];
    }
    //  print_array(c, "c");
    first_index += buffer;
    last_index += buffer;
    printf("%i\t%i\t%i\n", first_index, buffer, last_index);
    return NULL;
}

// //  define function to calculate the dot product of two vectors with single-threaded execution
int dot_product_ste()
{
    for (int i = 0; i < array_size; i++)
    {
        sum += a[i] * b[i];
    }
    return sum;
}

//  define main
int main()
{
    srand(time(NULL));
    pthread_t threads[] = {0, 1, 2, 3, 4, 5, 6, 7};
    int c;
    last_index = buffer;

    //  generate randomly integer array a
    random_int_generator(a);
    //  print_array(a, "a");

    //  generate randomly integer array b
    random_int_generator(b);
    //  print_array(b, "b");

    clock_t begin = clock();

    printf("Single-threaded execution\n");
    //  calculate the dot product
    c = dot_product_ste(a, b);
    printf("c = %i\n", c);

    clock_t end = clock();
    double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("Time elapsed:\t%lf\n", time_spent);

    begin = clock();

    printf("Multithreaded execution\n");
    for (int i = 0; i < number_of_threads; i++)
    {
        pthread_create(&threads[i], NULL, dot_product, NULL);
    }
    //  for (int i = 0; i < number_of_threads; i++)
    //  {
    //      pthread_join(threads[i], NULL);
    //  }
    printf("c = %i\n", c);

    end = clock();
    time_spent = (double)(end - begin) / CLOCKS_PER_SEC;

    printf("Time elapsed:\t%lf\n", time_spent);

    return 0;
}
