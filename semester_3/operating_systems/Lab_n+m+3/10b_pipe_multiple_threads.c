/* second pipe example: fork/read/write within same process */
/*      (see pages 40-46 Unix Net Progr by RW Stevens)      */

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

/* Read characters from the pipe and echo them to stdout. */
void read_from_pipe(int file)
{
	FILE *stream;
	int c;
	char ptr[100];
	stream = fdopen(file, "r");
	//while ((c = fgetc (stream)) != EOF) {
	//  printf(" \n ");
	// putchar (c);
	//}
	while (fgets(ptr, 100, stream) != NULL)
		//puts (ptr);
		printf(" read message:%s\n", ptr);
	fclose(stream);
}

/* Write some random text to the pipe. */
void write_to_pipe(int file)
{
	// use strem instead of char inbuf[MSGSIZE]
	FILE *stream;
	stream = fdopen(file, "w");
	fprintf(stream, "hello, world!\n");
	fprintf(stream, "goodbye, world!\n");
	fclose(stream);
}

int main(void)
{

	pid_t pid;	 // pid used to get fork() return value
	int parent[2]; // standard pipe definition -> file descriptor p[0] for reading and p[1] for writing
	int child[2];

	int num;

	/* Create the pipe using pipe */
	if (pipe(parent))
	{ /* open pipe with the p[2] descriptor */
		fprintf(stderr, "Paraent pipe failed.\n");
		return EXIT_FAILURE;
	}

	/* Create the pipe using pipe */
	if (pipe(child))
	{ /* open pipe with the p[2] descriptor */
		fprintf(stderr, "Child pipe failed.\n");
		return EXIT_FAILURE;
	}

	/* now pipe is created, so fork to create double linked pipe */
	/* Create the child process. */
	pid = fork();
	if (pid == (pid_t)0)
	{ // child process

		/* close child write to pipe - so only parent can write to pipe (child can read from it) */
		close(parent[1]);
		close(child[0]);
		write_to_pipe(child[1]);
		read_from_pipe(parent[0]);
		exit(0);
		//return EXIT_SUCCESS;
	}
	else if (pid < (pid_t)0)
	{

		/* The fork failed */
		fprintf(stderr, "Fork failed.\n");
		return EXIT_FAILURE;
	}
	else
	{ // parent  process

		/* close parent read from pipe - so only child can read from pipe (parent can write to it) */
		close(parent[0]);
		close(child[1]);
		write_to_pipe(child[0]);
		read_from_pipe(parent[1]);



		wait(0);
		//return EXIT_SUCCESS;
	}
}
