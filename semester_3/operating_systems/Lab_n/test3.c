#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{
    int b, c;

    b = fork();

    if (b == -1)
    {
        fprintf(stderr, "error while creating process\b");
    }
    else if (b == 0)
    { // child process
        printf("b,\tchild process:\t%i\n", getpid());
        exit(0);
    }
    else if (b > 0)
    { // parent process
        printf("main,\tparent process:\t%i\n", getpid());

        c = fork();

        if (c == -1)
        {
            fprintf(stderr, "error while creating process\b");
        }
        else if (c == 0)
        { // child process
            printf("c,\tchild process:\t%i\n", getpid());
            exit(0);
        }
        else if (c > 0)
        { // parent process
            printf("main,\tparent process:\t%i\n", getpid());
            wait(0);
            wait(0);
        }
    }

    return 0;
}
