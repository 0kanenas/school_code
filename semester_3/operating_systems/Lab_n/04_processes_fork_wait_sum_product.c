// VIVLIOGRAFIA WS TO TELOS TOU MAJHMATOS
// 1) W. Richard Stevens and Stephan A. Rago, "Advanced Programming
// in the Unix Environment", Addison Wesley, 2014, 3rd edition
// to opoio einai to PLEON katallhlo
// 2) An jelete na diavasete perissotera deite p.q. search TUTORIALS
// apo to site http://www.yolinux.com/

// To parakatw programmataki ftiaqnei 2 processes, opou
// to process gonios upologizei to sum kai to process paidi
// upologizei to product 6 arijmwn.
// Diavaste to API gia fork/wait sunarthseis apo to kefalaio 9 tou
// vivliou (1), kuriws selides 228-234 kai 238-239. Epishs uparqei
// antistoiqo sto yolinux apo to tutorial
// http://www.yolinux.com/TUTORIALS/ForkExecProcesses.html

// Melethste ti ja sumbei sto paidi
// 1) an o gonios pejanei prin termatisei to paidi
//    (p.q. an ginei kill to parent process)
// 2) an o gonios den kalesei thn wait(0)
//    (diavasete gia orphan kai zombie processes)

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{

	int A[] = {1, 2, 3, 4, 5, 6}, i, sum = 0, product = 1, n;

	n = fork();

	if (n == -1)
	{
		fprintf(stderr, "problem creating a process\n");
	}
	else if (n == 0)
	{ // child process
		for (i = 0; i < 6; i++)
		{
			sum += A[i];
		}
		printf("This is child process %i computed sum %i\n", getpid(), sum);
		// sleep(40);
		exit(0);
	}
	else if (n > 0)
	{ // parent process
		for (i = 0; i < 6; i++)
		{
			product *= A[i];
		}
		printf("The parent process %i completed the product %i (child %i computes the sum)\n", getpid(), product, n);
		// sleep(40);
		wait(0);
	}
	
	return 0;
}
