#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(void)
{
    fork();
    fork();
    fork();
    printf("Parrent ID: %i, Child ID: %i\n", getppid(), getpid());
    sleep(10);

    return 0;
}