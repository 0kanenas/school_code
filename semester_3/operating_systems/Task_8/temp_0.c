#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>

int pfork(int x)
{
    for (int j = 1; j <= x; j++)
    {
        if (fork() == 0)
        {
            return j;
        }
    }
    return 0;
}

void pjoin(int x, int id)
{
    if (id == 0)
    {
        for (int j = 1; j <= x; j++)
        {
            wait(0);
        }
    }
    else
    {
        exit(0);
    }
}

int main()
{
    //  char *processes[3] = {"ps", "–ef", "sort", "wc –l"};
    char *process0[] = {"ps", "-ef", NULL};
    char *process1[] = {"sort", NULL};
    char *process2[] = {"wc", "-l", NULL};
    int ab[2], bc[2];
    pipe(ab);
    pipe(bc);

    pid_t myid = pfork(2);
    int n;

    if (myid == 0) //  parent process
    {
        close(ab[0]);         //  Close reading end of ab
        close(bc[0]);         //  Close reading end of bc
        close(bc[1]);         //  Close writing end of bc
        close(STDOUT_FILENO); //  Close STDOUT_FILENO

        dup2(ab[1], STDOUT_FILENO); //  Process A writes to process B

        //  printf("executing process0\n");
        execv("/bin/ps", process0); //  Execute process0

        close(ab[1]); //  Close writing end of ab

        exit(0);
    }
    else if (myid == 1) // first child process
    {
        close(ab[1]);        //  Close writing end of ab
        close(bc[0]);        //  Close reading end of bc
        close(STDIN_FILENO); //  Close STDIN_FILENO

        dup2(ab[0], STDIN_FILENO);  // Process B reads from process A
        dup2(bc[1], STDOUT_FILENO); // Process B writes to process C

        //  printf("executing process1\n");
        execv("/bin/sort", process1); //  Execute process1

        close(ab[0]); //  Close reading end of ab
        close(bc[1]); //  Close writing end of bc

        exit(0);
    }
    else if (myid == 2) //  second child process
    {
        close(ab[0]);        //  Close reading end of ab
        close(ab[1]);        //  Close writing end of ab
        close(bc[1]);        //  Close writing end of ab
        close(STDIN_FILENO); //  Close STDIN_FILENO

        dup2(bc[0], STDIN_FILENO); // Process C reads from process B

        //  printf("executing process2\n");
        execv("/bin/wc", process2); //  Execute process2

        close(bc[0]); //  Close reading end of bc

        exit(0);
    }

    pjoin(2, myid);

    return 0;
}
