/*  TASK
Create 3 processes and 2 separate pipes for unidirectional communication
among them in order to implement using execv a bash shell pipe command,
as shown below
/usr/bin/ps –ef | sort | wc –l 
*/
/*  NOTES

*/

#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>

int main()
{
    int pipe1[2], pipe2[2];
    char *processes[3] = {"/usr/bin/ps –ef", "sort", "wc –l"};

    pid_t fork1, fork2;

    if (pipe(pipe1) == -1)
    {
        fprintf(stderr, "pipe1 failed.");
        return 1;
    }
    if (pipe(pipe2) == -1)
    {
        fprintf(stderr, "pipe2 failed.");
        return 1;
    }

    fork1 = fork();
    if (fork1 < 0)
    {
        fprintf(stderr, "fork1 failed.");
        return 1;
    }
    else if (fork1 > 0) //  parent=fork1
    {
        close(pipe1[0]);                                         //  close reading end of pipe1
        write(pipe1[1], processes[0], strlen(processes[0]) + 1); //  write processes 0 in pipe1
        close(pipe1[1]);                                         //  close writing end of pipe1

        execv(processes[0], processes);

        close(pipe2[1]);                                        //  close writing end of pipe2
        read(pipe2[0], processes[1], strlen(processes[1]) + 1); //  read processes 1 in pipe2
        close(pipe2[0]);                                        //  close reading end of pipe2

        //        if (fork2 < 0)
        //        {
        //            fprintf(stderr, "fork2 failed.");
        //            return 1;
        //        }
        //        else if (fork2 > 0) //  parent=fork2
        //        {
        //        }
        //        else //  child=fork2
        //        {
        //        }
    }
    else // child=fork1
    {
        close(pipe1[1]);                                        //  close writing end of pipe 1
        read(pipe1[0], processes[0], strlen(processes[0]) + 1); //  read processes 0

        execv(processes[0], processes);

        close(pipe1[0]); //  close reading end of pipe1
        close(pipe2[0]); //  close reading end of pipe2

        write(pipe2[1], processes[1], strlen(processes[1]) + 1); //  write processes 1
        close(pipe2[1]);                                         // close writing end of pipe 2
    }

    return 0;
}
