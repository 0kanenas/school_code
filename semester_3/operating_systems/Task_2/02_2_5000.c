#include <time.h>
#include <stdint.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

#define N 100
int random_int_generator(int[]);

int main()
{
    srand(time(NULL));
    int x[N], a, b, c, d;
    int index, prod_odd = 1, sum_odd = 0;
    random_int_generator(x);

    if (a == -1)
    {
        fprintf(stderr, "error while creating process\b");
    }
    else if (a == 0)
    { // child process
        printf("a,\tchild process:\t%i\n", getpid());
        for (index = 0; index < N; index++)
        {
            if (x[index] % 2 == 1)
            {
                prod_odd *= x[index];
            }
        }
        exit(0);
    }
    else if (a > 0)
    { // parent process
        printf("main,\tparent process:\t%i\n", getpid());

        b = fork();

        if (b == -1)
        {
            fprintf(stderr, "error while creating process\b");
        }
        else if (b == 0)
        { // child process
            printf("b,\tchild process:\t%i\n", getpid());
            for (index = 0; index < N; index++)
            {
                if (x[index] % 2 == 1)
                {
                    prod_odd += x[index];
                }
            }
            d = fork();

            if (d == -1)
            {
                fprintf(stderr, "error while creating process\b");
            }
            else if (d == 0)
            { // child process
                printf("d,\tchild process:\t%i\n", getpid());
                exit(0);
            }
            else if (d > 0)
            { // parent process
                printf("main,\tparent process:\t%i\n", getpid());
                for (index = 0; index < N; index++)
                {
                    if (x[index] % 2 == 1)
                    {
                        prod_odd *= x[index];
                    }
                }
                wait(0);
                exit(0);
            }
            else if (b > 0)
            { // parent process
                printf("main,\tparent process:\t%i\n", getpid());

                c = fork();

                if (c == -1)
                {
                    fprintf(stderr, "error while creating process\b");
                }
                else if (c == 0)
                { // child process
                    printf("c,\tchild process:\t%i\n", getpid());
                    for (index = 0; index < N; index++)
                    {
                        if (x[index] % 2 == 1)
                        {
                            prod_odd += x[index];
                        }
                    }
                    exit(0);
                }
                else if (c > 0)
                { // parent process
                    printf("main,\tparent process:\t%i\n", getpid());
                    wait(0);
                    wait(0);
                    wait(0);
                }
            }
        }
    }

    printf("Sum odd:\t%i\n", sum_odd);
    printf("Prod odd:\t%i\n", prod_odd);

    return 0;
}

int random_int_generator(int arr[])
{
    for (int index = 0; index < N; index++)
    {
        arr[index] = rand() % INT16_MAX + INT16_MIN;
    }
}