#!/bin/bash

read -p 'Type something:    ' var

if [ $var == "file" ]; then
    echo "Files found:"
    find *.txt
    echo "Last modified at:"
    for entry in $(find *.txt); do
        stat -c %y "$entry"
    done
    echo "Alphabetically sorted:"
    find *.txt | sort
elif [ $var == "dir" ]; then
    echo "Subdirectories in this directory:"
    ls -l | grep "^d" | wc -l
else
    echo "error"
fi

#   Thanks StackOverflow exists.
