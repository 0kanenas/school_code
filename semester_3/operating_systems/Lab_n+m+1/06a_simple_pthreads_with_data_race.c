// Program with inconsistent behavior (data race)
// Create 3 threads with pthread_create kai pthread_join

//LAB:
// 1) Compare mumber of data races when pthread_yield() is used
// x=1000; while [ $x -ge 0 ] ; do ./a.out ; let x=$x-1; done | grep "x:" | grep -v 300 | wc -l
//
// Can we always get rid of data races, using sleep?
// (what happens when lots of data are accessed, e.g. 100 -> 10000)

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pthread.h>

int x = 0; // global
//pthread_mutex_t L = PTHREAD_MUTEX_INITIALIZER;

void *addtoX(void *arg)
{
	//int temp;
	int i, width=10;
	int id = (intptr_t)arg;
	printf("Before sum:\t%*i\tThread argument id:\t%i and thread id:\t%ld\n", width, i, id, pthread_self());
	//sleep(10);
	for (i = 0; i < 100; i++)
	{
		//int temp = x;
		//temp++;
		//pthread_yield(); // yield to other threads
		//x = temp;

		//pthread_mutex_lock(&L);
		x = x + 1;
		//printf("y:%d \n", x);
		//fflush(stdout);
		//pthread_mutex_unlock(&L);
	}
	printf("After sum:\t%*i\tThread argument id:\t%i and thread id:\t%ld\n", width, i, id, pthread_self());
	return 0;
}

int main()
{

	pthread_t tid[3];
	int i;

	// threads creation
	for (i = 0; i < 3; i++)
		pthread_create(&tid[i], NULL, addtoX, (void *)(intptr_t)i);

	// wait for all three threads to terminate
	for (i = 0; i < 3; i++)
		pthread_join(tid[i], NULL);

	printf("x:%d \n", x);
	fflush(stdout);

	return 0;
}
