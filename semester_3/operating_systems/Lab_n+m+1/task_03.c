#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define AS 5  //  array size
#define NoT 8 // NoT = Number of Threads
#define N 100

int random_int_generator(int arr[])
{
    for (int index = 0; index < AS; index++)
    {
        arr[index] = rand() % N;
    }
    return 0;
}

int a[AS], b[AS], dotprod_ab[NoT];
random_int_generator(a);
random_int_generator(b);
int part = 0;

void *series(void *arg)
{
    //  Each thread computes dot product of 1/8th of array
    int thread_part = part++;

    for (int i = thread_part * (AS / NoT); i < (thread_part + 1) * (AS / NoT); i++)
    {
        dotprod_ab[thread_part] += a[i] * b[i];
    }
    return 0;
}

int print_array(int arr[], char name)
{
    for (int i = 0; i < AS; i++)
    {
        printf("%s[%i] = %i\n", name, i, arr[i]);
    }
}

int main()
{
    pthread_t threads[NoT];

    //  Creating threads
    for (int i = 0; i < NoT; i++)
    {
        pthread_create(&threads[i], NULL, series, (void *)NULL);
    }

    //  Joining threads
    for (int i = 0; i < NoT; i++)
    {
        pthread_join(threads[i], NULL);
    }

    //  Adding sum of all 8 parts
    int series_sum = 0;
    for (int i = 0; i < NoT; i++)
    {
        series_sum += dotprod_ab[i];
    }

    print_array(a, 'a');
    print_array(b, 'b');
    print_array(dotprod_ab, 'dotprod_ab');

    return 0;
}
