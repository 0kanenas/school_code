#include <sys/ipc.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define N 10

int random_int_generator(int[]);
int print_array(int[]);

int main()
{
	srand(time(NULL));

	int arr[N];
	int sum = 0, i;
	int *ptr;
	int parent_id, shmid;

	int pfork(int);
	void pjoin(int, int);

	random_int_generator(arr);

	shmid = shmget(IPC_PRIVATE, 4, IPC_CREAT | 0666);
	if (shmid == -1)
	{
		printf("Error allocating memory segment.\n");
		exit(1);
	}
	else
	{
		printf("shmid:\t\t%i\n", shmid);
	}

	ptr = shmat(shmid, 0, 0);
	*ptr = 0;

	parent_id = pfork(1);

	if (parent_id == 0)
	{
		for (i = 0; i < N; i += 2)
		{
			sum = sum + arr[i];
		}
		printf("Parent sum:\t%i\n", sum);
	}
	else if (parent_id == 1)
	{
		for (i = 1; i < N; i += 2)
		{
			*ptr = *ptr + arr[i];
		}
		printf("Child sum:\t%i\n", *ptr);
	}

	pjoin(1, parent_id);

	sum = sum + *ptr;

	system("ipcs");

	shmdt(ptr);

	shmctl(shmid, IPC_RMID, NULL);

	sleep(5);

	system("ipcs");

	printf("Total sum:\t%i\n", sum);
	return 0;
}

int random_int_generator(int arr[])
{
	for (int index = 0; index < N; index++)
	{
		arr[index] = rand() % N;
	}
}

int print_array(int arr[])
{
	printf("Printing array.\n");
	for (int i = 0; i < N; i++)
	{
		printf("arr[%i] = %i\n", i, arr[i]);
	}
	return (0);
}

int pfork(int x)
{
	int j;
	for (j = 1; j <= x; j++)
	{
		if (fork() == 0)
		{
			return j;
		}
	}
	return 0;
}

void pjoin(int x, int id)
{
	int j;
	if (id == 0)
	{
		for (j = 1; j <= x; j++)
		{
			wait(0);
		}
	}
	else
	{
		exit(0);
	}
}
