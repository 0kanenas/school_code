#include <sys/ipc.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#define N 10

int random_int_generator(int[]);
int print_array(int[]);

int main()
{
    srand(time(NULL));

    int arr[N];
    
    random_int_generator(arr);
    print_array(arr);
    
    return 0;
}

int random_int_generator(int arr[])
{
    for (int index = 0; index < N; index++)
    {
        arr[index] = rand() % N;
    }
    return 0;
}

int print_array(int arr[])
{
    for (int i = 0; i < N; i++)
    {
        printf(" arr[%i] = %i\n", i, arr[i]);
    }
    return (0);
}