#include <stdio.h>
#include <time.h>

#define N 1000000

int main()
{
    clock_t time;
    time = clock();

    double time_taken;

    int X[N], A[N], i, s;

    s = 0;
    for (i = 0; i < N; i++)
    {
        s += X[i];
        A[i] = s;
    }

    time = clock() - time;
    time_taken = (double)time / CLOCKS_PER_SEC;

    printf("%lf seconds\n", time_taken);

    return 0;
}
