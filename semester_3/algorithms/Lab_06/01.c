#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>

#define N 10

int random_int_generator(int arr[])
{
	for (int index = 0; index < N; index++)
	{
		arr[index] = rand() % N + 0;
	}
}

void merge(int *Arr, int start, int mid, int end)
{
	int temp[end - start + 1];

	int i = start, j = mid + 1, k = 0;

	while (i <= mid && j <= end)
	{
		if (Arr[i] <= Arr[j])
		{
			temp[k] = Arr[i];
			k += 1;
			i += 1;
		}
		else
		{
			temp[k] = Arr[j];
			k += 1;
			j += 1;
		}
	}

	while (i <= mid)
	{
		temp[k] = Arr[i];
		k += 1;
		i += 1;
	}

	while (j <= end)
	{
		temp[k] = Arr[j];
		k += 1;
		j += 1;
	}

	for (i = start; i <= end; i += 1)
	{
		Arr[i] = temp[i - start];
	}
}

void mergeSort(int *Arr, int start, int end)
{
	if (start < end)
	{
		int mid = (start + end) / 2;
		mergeSort(Arr, start, mid);
		mergeSort(Arr, mid + 1, end);
		merge(Arr, start, mid, end);
	}
}

int main()
{
	srand(time(NULL));
	int arr[N], i;
	random_int_generator(arr);
	printf("First %i items of unsorted array:\n", N);
	for (i = 0; i < N; i++)
	{
		printf("%i\n", arr[i]);
	}
	mergeSort(arr, 0, N);
	printf("First %i items of sorted array:\n", N);
	for (i = 0; i < N; i++)
	{
		printf("%i\n", arr[i]);
	}

	return 0;
}