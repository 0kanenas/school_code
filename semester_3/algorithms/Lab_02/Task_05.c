#include <stdio.h>

void temp(int *, int *);

int main()
{
    int a, b;
    printf("Enter two integers:\n");
    printf("a: ");
    scanf("%d", &a);
    printf("b: ");
    scanf("%d", &b);
    
    for (int i = 0; i<10; i++)
    {
        printf("-");
    }
    printf("\n");

    printf("before\n");
    printf("a: %d, b: %d\n", a, b);

    temp(&a, &b);

    printf("after\n");
    printf("a: %d, b: %d\n", a, b);

    return 0;
}

void temp(int *a, int *b)
{
    int c;
    c = *a;
    *a = *b;
    *b = c;
}