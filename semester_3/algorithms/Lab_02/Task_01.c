#include <stdio.h>

int calculator(int, int, char);

int main()
{
    int a, b;
    char ch;

    while (1)
    {
        printf("Enter an operation sign and two integers:\n");

        scanf(" %c", &ch);
        scanf("%d", &a);
        scanf("%d", &b);

        if (((ch == '+') || (ch == '-')) || ((ch == '*') || (ch == '/')))
        {
            calculator(a, b, ch);
            printf("\n\n");
        }
    }

    return 0;
}

int calculator(int a, int b, char ch)
{
    if (ch == '+')
    {
        int c = a + b;
        printf("%d + %d = %d", a, b, c);
    }
    if (ch == '-')
    {
        int c = a - b;
        printf("%d - %d = %d", a, b, c);
    }
    if (ch == '*')
    {
        int c = a * b;
        printf("%d * %d = %d", a, b, c);
    }
    if (ch == '/')
    {
        if (b != 0)
        {
            float c = (float)a / (float)b;
            printf("%d / %d = %.2f", a, b, c);
        }
        else
        {
            printf("Denominator is equal to 0.");
        }
    }
}
