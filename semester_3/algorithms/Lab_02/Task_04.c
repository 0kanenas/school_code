#include <stdio.h>

int fibonacci(int);

int main()
{
    int n;
    while (1)
    {
        printf("Enter a non negative integer:\t");
        scanf("%d", &n);
        if (n > 0)
        {
            break;
        }
    }

    printf("%d\n", fibonacci(n));

    return 0;
}

int fibonacci(int n)
{
    if (n == 1)
    {
        return 0;
    }
    else
    {
        if (n == 2)
        {
            return 1;
        }
        else
        {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }
}