#include <stdio.h>

long double power(int, float);

int main()
{
    int ak;
    float bs;

    while (1)
    {
        printf("Enter the base:\n");
        scanf("%f", &bs);
        if (bs == 0)
        {
            printf("Base is equal to 0.\n");
        }
        else
        {
            break;
        }
    }

    printf("Enter the power:\n");
    scanf("%d", &ak);

    printf("%f ^ %d = %Lf\n", bs, ak, power(ak, bs));

    return 0;
}

long double power(int ak, float bs)
{
    long double result;

    if (ak == 0)
    {
        result = 1;
    }
    else
    {
        if (ak > 0)
        {
            result = bs;
            for (int i = 0; i < ak - 1; i++)
            {
                result *= result;
            }
        }
        else
        {
            result = bs;
            for (int i = 0; i < -ak - 1; i++)
            {
                result *= result;
            }
            result = 1 / result;
        }
    }
    return result;
}