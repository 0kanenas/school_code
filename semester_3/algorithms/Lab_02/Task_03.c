#include <stdio.h>

unsigned long long int factorial(int);

int main()
{
    int n;
    while (1)
    {
        printf("Enter a non negative integer:\t");
        scanf("%d", &n);
        if (n >= 0)
        {
            break;
        }
    }

    printf("%d! = %llu\n", n, factorial(n));

    return 0;
}

unsigned long long int factorial(int n)
{
    if (n == 1)
    {
        return n;
    }
    else
    {
        return n * factorial(n - 1);
    }
}
