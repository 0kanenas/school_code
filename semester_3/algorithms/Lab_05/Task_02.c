#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>

#define N 100000

int random_int_generator(int[]);
void selection_sort(int[]);
void swap(int *, int *);

int main()
{
    srand(time(NULL));

    int i, arr[N];

    random_int_generator(arr);
    /*
    printf("Unsorted\n");
    for (i = 0; i < 10; i++)
    {
        printf("%i\t%i\n", i, arr[i]);
    }
    */

    selection_sort(arr);
    /*
    printf("Sorted\n");
    for (i = 0; i < 10; i++)
    {
        printf("%i\t%i\n", i, arr[i]);
    }
    */

    return 0;
}

int random_int_generator(int arr[])
{
    for (int index = 0; index < N; index++)
    {
        arr[index] = rand() % INT8_MAX + INT8_MAX;
    }
}

void selection_sort(int arr[])
{
    int i, j, min_idx;
    for (i = 0; i < N - 1; i++)
    {
        min_idx = i;
        for (j = i + 1; j < N; j++)
            if (arr[j] < arr[min_idx])
                min_idx = j;
        swap(&arr[min_idx], &arr[i]);
    }
}

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}
