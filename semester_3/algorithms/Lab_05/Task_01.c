#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>

#define N 100000

int random_int_generator(int[]);
void insertion_sort(int[]);

int main()
{
    srand(time(NULL));

    int i, arr[N];

    random_int_generator(arr);
    /*
    printf("Unsorted\n");
    for (i = 0; i < 10; i++)
    {
        printf("%i\t%i\n", i, arr[i]);
    }
    */

    insertion_sort(arr);
    /*
    printf("Sorted\n");
    for (i = 0; i < 10; i++)
    {
        printf("%i\t%i\n", i, arr[i]);
    }
    */

    return 0;
}

int random_int_generator(int arr[])
{
    for (int index = 0; index < N; index++)
    {
        arr[index] = rand() % INT8_MAX + INT8_MAX;
    }
}

void insertion_sort(int arr[])
{
    int i, key, j;
    for (i = 1; i < N; i++)
    {
        key = arr[i];
        j = i - 1;
        while (j >= 0 && arr[j] > key)
        {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}
