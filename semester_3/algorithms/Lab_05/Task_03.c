#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>

#define N 100000

int random_int_generator(int[]);
void bubble_sort(int[]);
void swap(int *, int *);

int main()
{
    srand(time(NULL));

    int i, arr[N];

    random_int_generator(arr);
    /*
    printf("Unsorted\n");
    for (i = 0; i < 10; i++)
    {
        printf("%i\t%i\n", i, arr[i]);
    }
    */

    bubble_sort(arr);
    /*
    printf("Sorted\n");
    for (i = 0; i < 10; i++)
    {
        printf("%i\t%i\n", i, arr[i]);
    }
    */

    return 0;
}

int random_int_generator(int arr[])
{
    for (int index = 0; index < N; index++)
    {
        arr[index] = rand() % INT8_MAX + INT8_MAX;
    }
}

void bubble_sort(int arr[])
{
    int i, j;
    for (i = 0; i < N - 1; i++)
        for (j = 0; j < N - i - 1; j++)
            if (arr[j] > arr[j + 1])
                swap(&arr[j], &arr[j + 1]);
}

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *yp = temp;
}
