#include <stdio.h>

int main()
{
    float below, sequence_1, sequence_2;
    int i, n;
    below = sequence_1 = sequence_2 = 1.0;
    n = 20;
    printf("%d\n", n);
    for (i = 0; i < n; i++)
    {
        sequence_1 += 1.0 / below;
        below++;
    }
    printf("%f\n", sequence_1);
    for (i = 0; i < n; i++)
    {
        if (i % 2 != 0)
        {
            sequence_2 += 1.0 / below;
        }
        else
        {
            sequence_2 -= 1.0 / below;
        }
        below++;
    }
    printf("%f\n", sequence_2);

    n = 100;
    printf("\n%d\n", n);
    for (i = 0; i < n; i++)
    {
        sequence_1 += 1.0 / below;
        below++;
    }
    printf("%f\n", sequence_1);
    for (i = 0; i < n; i++)
    {
        if (i % 2 != 0)
        {
            sequence_2 += 1.0 / below;
        }
        else
        {
            sequence_2 -= 1.0 / below;
        }
        below++;
    }
    printf("%f\n", sequence_2);

    n = 500;
    printf("\n%d\n", n);
    for (i = 0; i < n; i++)
    {
        sequence_1 += 1.0 / below;
        below++;
    }
    printf("%f\n", sequence_1);
    for (i = 0; i < n; i++)
    {
        if (i % 2 != 0)
        {
            sequence_2 += 1.0 / below;
        }
        else
        {
            sequence_2 -= 1.0 / below;
        }
        below++;
    }
    printf("%f\n", sequence_2);

    n = 1000;
    printf("\n%d\n", n);
    for (i = 0; i < n; i++)
    {
        sequence_1 += 1.0 / below;
        below++;
    }
    printf("%f\n", sequence_1);
    for (i = 0; i < n; i++)
    {
        if (i % 2 != 0)
        {
            sequence_2 += 1.0 / below;
        }
        else
        {
            sequence_2 -= 1.0 / below;
        }
        below++;
    }
    printf("%f\n", sequence_2);

    return 0;
}
