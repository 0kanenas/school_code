#include <stdio.h>
#include <string.h>

#define N 50

void print_strings(char[], char[]);

int main()
{
    char pin[N], mat[N];

    printf("Enter two character arrays:\n");

    fgets(pin, sizeof(pin), stdin);
    fgets(mat, sizeof(mat), stdin);

    printf("First character array:\t%s", pin);
    printf("Second character array:\t%s", mat);

    if (strlen(pin) < strlen(mat))
    {
        strcpy(mat, pin);
    }
    else
    {
        strcpy(pin, mat);
    }
    
    printf("First character array:\t%s", pin);
    printf("Second character array:\t%s", mat);

    return 0;
}