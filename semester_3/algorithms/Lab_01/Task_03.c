#include <stdio.h>

#define N 5

int min(int[]);
int max(int[]);

int main()
{
    int ints[N], i;
    printf("Enter %d integers\n", N);
    for (i = 0; i < N; i++)
    {
        scanf("%d", ints + i);
    }

    printf("Minimum integer:\t%d\n", ints[min(ints)]);
    printf("Maximum integer:\t%d\n", ints[max(ints)]);
    return 0;
}

int min(int array[])
{
    int c, minimum, index;

    minimum = array[0];
    index = 0;

    for (c = 1; c < N; c++)
    {
        if (array[c] < minimum)
        {
            index = c;
            minimum = array[c];
        }
    }

    return index;
}

int max(int array[])
{
    int c, maximum, index;

    maximum = array[0];
    index = 0;

    for (c = 1; c < N; c++)
    {
        if (array[c] > maximum)
        {
            index = c;
            maximum = array[c];
        }
    }

    return index;
}
