#include <stdio.h>

int main()
{
    int k, l;

    for (k = 0; k < 5; k++)
    {
        for (l = 1; l <= 9; l++)
        {
            printf("%d", l);
        }
        printf("\n");
    }

    for (k = 0; k < 20; k++)
    {
        printf("-");
    }
    printf("\n");

    for (k = 0; k < 5; k++)
    {
        for (l = k + 1; l <= 9; l++)
        {
            printf("%d", l);
        }
        printf("\n");
    }

    return 0;
}
