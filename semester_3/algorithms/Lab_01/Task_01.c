#include <stdio.h>

#define N 4

int main()
{
    int i;
    float num, sum = 0;
    printf("Enter %d floats.\n", N);
    for (i = 0; i < N; i++)
    {
        printf("Float %d:\t", i + 1);
        scanf("%f", &num);
        if (num > 0)
        {
            sum += num;
        }
    }
    printf("The sum of the given floats is:\t%f\n", sum);
    return 0;
}
