#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int main()
{
    srand(time(NULL));

    int num = rand(), gs, cnt = 0;

    printf("Enter an integer:\t");
    scanf("%d", &gs);

    while (gs != num)
    {
        cnt++;

        if (gs > num)
        {
            printf("Enter a smaller number.\n");
        }
        else
        {
            printf("Enter a larger number.\n");
        }

        printf("Enter an integer:\t");
        scanf("%d", &gs);
    }

    printf("Number of tries:\t%d\nRandom number:\t%d\n", cnt, num);

    return 0;
}
