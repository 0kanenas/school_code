#include <stdio.h>
#include <string.h>

#define N 100

int main()
{
    char FullName[N];
    int Index, CharacterCounter, FirstNameCharacterCounter, LastNameCharacterCounter, Diff = 2;
    Index = CharacterCounter = FirstNameCharacterCounter = 0;

    printf("Enter your full name:\t");
    fgets(FullName, sizeof(FullName), stdin);

    //  Count characters before the space
    while (Index < N)
    {
        if (FullName[Index] != ' ')
        {
            CharacterCounter++;
        }
        else
        {
            break;
        }
        Index++;
    }

    FirstNameCharacterCounter = CharacterCounter;

    //  Count characters staring where left of and substract 1 because of the Enter
    while (Index < N)
    {
        if (FullName[Index] != '\0')
        {
            CharacterCounter++;
        }
        else
        {
            CharacterCounter--;
            break;
        }
        Index++;
    }

    LastNameCharacterCounter = CharacterCounter - FirstNameCharacterCounter - 1;

    printf("%s", FullName);

    for (Index = 0; Index < FirstNameCharacterCounter - 1; Index++)
    {
        printf(" ");
    }
    printf("%d", FirstNameCharacterCounter);

    if (LastNameCharacterCounter > 9)
    {
        Diff = 3;
    }

    for (Index; Index < CharacterCounter - Diff; Index++)
    {
        printf(" ");
    }
    printf("%d\n", LastNameCharacterCounter);

    return 0;
}
