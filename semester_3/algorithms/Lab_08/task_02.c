#include <stdio.h>
#include <stdlib.h>

#define N 20

typedef struct node
{
    int vertex;
    struct node *next;
} node;

void read_graph(struct node *graph[], int non)
{
    struct node *new_node;
    int i, j, k, val;
    for (i = 0; i < non; i++)
    {
        struct node *last = NULL;
        printf("Enter the number of neighbours of %i:\t", i + 1);
        scanf("%i", &k);
        for (j = 0; j < k; j++)
        {
            printf("Enter the value of %i neighbour of %i:\t", j + 1, i + 1);
            scanf("%i", &val);
            new_node = (struct node *)malloc(sizeof(struct node *));
            new_node->vertex = val;
            new_node->next = NULL;
            if (graph[i] == NULL)
            {
                graph[i] = new_node;
            }
            else
            {
                last->next = new_node;
            }
            last = new_node;
        }
    }
}

void print_graph(struct node *graph[], int non)
{
    struct node *ptr = NULL;
    int i, j;
    for (i = 0; i < non; i++)
    {
        ptr = graph[i];
        printf("The neighbours of %i are:\t", i + 1);
        while (ptr != NULL)
        {
            printf("%i\t", ptr->vertex);
            ptr = ptr->next;
        }
        printf("\n");
    }
}

int main()
{
    int i, j, k, nodes;
    
    printf("Enter the nodes:\t");
    scanf("%i", &nodes);
    
    struct node *graph[nodes];
    
    for (i = 0; i < nodes; i++)
    {
        graph[i] = NULL;
    }
    
    read_graph(graph, nodes);
    print_graph(graph, nodes);
    
    printf("\n");

    return 0;
}
