/* ---------------
	Ergastirio 8
	Askisi 1
	
	Dimiourgia grafou me xrisi statherou disdiastatou pinaka
   --------------- */

#include <stdio.h>
#include <stdlib.h>

#define VERTICES 5

void readGraph(int[][VERTICES], int *);
void printGraph(int[][VERTICES]);
void dfs(int[][VERTICES], int);

int main()
{
	int graph[VERTICES][VERTICES], edges;

	readGraph(graph, &edges);

	printGraph(graph);
	dfs(graph, 1);
	
	return 0;
}

/* Create and read adjacent matrix for graph */
void readGraph(int graph[][VERTICES], int *edges)
{
	int i, j, vertex1, vertex2;

	printf("Enter number of edges: ");
	scanf("%i", edges);

	// Initialize graph
	for (i = 0; i < VERTICES; i++)
	{
		for (j = 0; j < VERTICES; j++)
		{
			graph[i][j] = 0;
		}
	}

	// Read adjacents edges
	for (i = 0; i < *edges; i++)
	{
		printf("\nEnter adjacent edges: ");
		scanf("%i %i", &vertex1, &vertex2);
		graph[vertex1][vertex2] = 1;
		graph[vertex2][vertex1] = 1;
	}
}

/* Print adjacent matrix for graph */
void printGraph(int graph[][VERTICES])
{
	int i, j;

	printf("\nAdjecency graph:\n");
	for (i = 0; i < VERTICES; i++)
	{
		for (j = 0; j < VERTICES; j++)
		{
			printf("%i ", graph[i][j]);
		}
		printf("\n");
	}
	printf("\n");
}

void dfs(int graph[][VERTICES], int v)
{
	int i, reach[VERTICES];
	reach[v] = 1;
	for (i = 1; i <= VERTICES; i++)
		if (graph[v][i] && !reach[i])
		{
			printf("\n%i->%i", v, i);
			dfs(graph[VERTICES][VERTICES], i);
		}
}
