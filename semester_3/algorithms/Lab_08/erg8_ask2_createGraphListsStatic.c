/* ---------------
	Ergastirio 8
	Askisi 2
	
	Dimiourgia grafou me xrisi listwn
	kai stathero pinaka deiktwn
   --------------- */

#include <stdio.h>
#include <stdlib.h>

#define VERTICES 5

typedef struct node
{
    struct node *next;
    int vertex;
} node;

int readGraph(node **);
int insert(node **, int, int);
void printGraph(node **);

int main()
{
    node *graph[VERTICES];

    if (readGraph(graph) == 1)
    {
        printf("malloc returned NULL\n");
        return 1;
    }
    printGraph(graph);

    return 0;
}

/* Create and read graph lists */
int readGraph(node **graph)
{
    int i, j, adjacentVertex, edges, insertReturnValue;

    for (i = 0; i < VERTICES; i++)
    {
        // Initialize graph with NULL
        graph[i] = NULL;

        // Read edges and insert them in graph[]
        printf("Enter number of edges for vertex %i: ", i);
        scanf("%i", &edges);

        for (j = 0; j < edges; j++)
        {
            printf("Enter adjacent vertex: ");
            scanf("%i", &adjacentVertex);
            insertReturnValue = insert(graph, i, adjacentVertex);
            if (insertReturnValue == 1)
            {
                return 1;
            }
        }
    }
    return 0;
}

/* Insert adjacent vertex to node (vertex) from graph */
int insert(node **graph, int vertex, int adjacentVertex)
{
    node *ptr, *newNode;

    // Allocate memory for the new node
    newNode = (node *)malloc(sizeof(node));
    if (newNode == NULL)
    {
        return 1;
    }
    newNode->vertex = adjacentVertex;
    newNode->next = NULL;

    // If vertex has no adjacent vertices
    if (graph[vertex] == NULL)
    {
        graph[vertex] = newNode;
    }
    else
    {
        // Go to the last adjacent vertex
        ptr = graph[vertex];
        while (ptr->next != NULL)
        {
            ptr = ptr->next;
        }
        ptr->next = newNode;
    }

    return 0;
}

/* Print graph vertices and their adjacent vertices */
void printGraph(node **graph)
{
    int i;
    node *ptr;

    printf("\nGraph nodes:\n");
    for (i = 0; i < VERTICES; i++)
    {
        printf("Node %d: ", i);
        ptr = graph[i];
        while (ptr != NULL)
        {
            printf("%i ", ptr->vertex);
            ptr = ptr->next;
        }
        printf("\n");
    }
    printf("\n");
}
