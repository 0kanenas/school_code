from os import system as run
from sys import argv

var = argv[1]

run(f'gcc {var} -std=c11 -pedantic-errors && ./a.out')
