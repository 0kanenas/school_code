#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>

#define N 100000

int binarySearch(int[], int, int);
int random_int_generator(int[]);

int main()
{
    srand(time(NULL));

    int array[N];
    random_int_generator(array);

    int query = rand() % INT16_MAX + INT16_MIN;
    int result = binarySearch(array, 0, query);

    (result == -1) ? printf("Query, %i, is not present in array",
                            query)
                   : printf("Query, %i, is present at index %d",
                            query, result);
    printf("\n");
    return 0;
}

// A iterative binary search function. It returns 
// location of query in given array arr[left..right] if present, 
// otherwise -1 
int binarySearch(int arr[], int left, int query) 
{
    int right = N;
    while (left <= right) { 
        int m = left + (right - left) / 2; 
  
        // Check if query is present at mid 
        if (arr[m] == query) 
            return m; 
  
        // If query greater, ignore left half 
        if (arr[m] < query) 
            left = m + 1; 
  
        // If query is smaller, ignore right half 
        else
            right = m - 1; 
    } 
  
    // if we reach here, then element was 
    // not present 
    return -1; 
} 

int random_int_generator(int array[])
{
    for (int index = 0; index < N; index++)
    {
        array[index] = rand() % INT16_MAX + INT16_MIN;
    }
}
