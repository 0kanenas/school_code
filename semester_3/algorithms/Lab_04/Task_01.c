#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdint.h>

#define N 100000

int random_int_generator(int []);
int serial_search(int[], int);

int main()
{
    srand(time(NULL));

    int array[N], array_index, query;

    random_int_generator(array);

    //  printf("Enter a number to search for in an array of random integers:\t");
    //  scanf("%i", &query);

    printf("Array size:\t%i\n", N);

    query = rand() % INT16_MAX + INT16_MIN;
    printf("Query:\t%i\n", query);

    array_index = serial_search(array, query);
    if (array_index != -1)
    {
        printf("Query, %i, is present at index %i\n", query, array_index);
        for (int i = array_index - 10; i < array_index + 10; i++)
        {
            if (i >= 0)
            {
                if (i == array_index)
                {
                    printf("%i, %i\n", i, array[i]);
                }
                else
                {
                    printf("\t%i, %i\n", i, array[i]);
                }
            }
        }
    }
    else
    {
        printf("Query, %i, is not present in array\n", query);
    }
}

int random_int_generator(int arr[])
{
    for (int index = 0; index < N; index++)
    {
        arr[index] = rand() % INT16_MAX + INT16_MIN;
    }
}

int serial_search(int arr[], int n)
{
    int index;
    for (index = 0; index < N; index++)
    {
        if (arr[index] == n)
        {
            return index;
        }
    }
    return -1;
}
