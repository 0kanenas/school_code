from os import system as run
from sys import argv
from time import sleep as wait

times = int(argv[1])
file = argv[2]

print(f'{times}')
print(f'{file}')

for time in range(0, times-1):
    run(f'gcc {file} -std=c11 -pedantic-errors && ./a.out')
    wait(1)
